($(document).on("ready", function() {

    $("div[data-src],a[data-src]").each(function() {
        setElementBackground.apply(this);
    });

    function setElementBackground() {
        var elem = $(this);
        var src = elem.data("src");
        elem.css("background-image", `url(${src})`);
    }

    // $(".switcher span").on("click", function() {
    //     var span = $(this);
    //     if (!span.hasClass("_selected")) {
    //         span.parent().find("span").removeClass("_selected");
    //         span.addClass("_selected");
    //         $(".services,.advantages").removeClass("_selected");
    //         $("." + span.data("toggle")).addClass("_selected");
    //     }
    // })

    if ($("#about-slider").length != 0) {
        var aboutSlider = $("#about-slider").lightSlider({
            controls: false,
            auto: true,
            pager: false,
            items: 3,
            pause: 10000,
            slideMargin: 30,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $('.slidePrev').click(function() {
            aboutSlider.goToPrevSlide();
        });

        $('.slideNext').click(function() {
            aboutSlider.goToNextSlide();
        });
    }


    $(".services-block--item").mouseenter(function() {
        var imageBlock = $(this).find(".services-block--item-img");
        var src = imageBlock.data("hover");
        console.log(src);

        imageBlock.css("background-image", `url(${src})`);
    })

    $(".services-block--item").mouseleave(function() {
        var imageBlock = $(this).find(".services-block--item-img");
        var src = imageBlock.data("src");
        console.log(src);
        imageBlock.css("background-image", `url(${src})`);
    })

    $(".services-switcher--item a").click(function(e) {
        e.preventDefault();
        var target = $(".services-block[data-content='" + $(this).data("target") + "']");
        $(".services-switcher--item").removeClass("_selected");
        $(".services-block").removeClass("_selected");
        $(this).parent().addClass("_selected");
        target.addClass("_selected");

    })



}))
